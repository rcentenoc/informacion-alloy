//signatures:
sig aLEJANDRO {}

sig B {
	parent: aLEJANDRO
}

// Predicates and facts
//pred show {}


//Assertions:

//Commands:
//run show for 3
run {} for exactly 2 aLEJANDRO, 1 B
//run {} for 3
//run atLeastObe
//run notASingleOne
