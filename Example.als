sig Student, Tutor, Mark {}

sig Course{
	reg : set Student,
	alloc : Student -> Tutor,
	result : Student -> Mark
	}

pred inv [c:Course] {
	(c.alloc).Tutor =c.reg
	(c.result).Mark in c.reg
	~(c.alloc).(c.alloc) in iden
	all s:Student | #s.(c.result) =< 1
	}

pred addReg [s:Student, t:Tutor, c,c':Course] {
	s !in c.reg
	c'.reg = c.reg ++s
	c'.alloc = c.alloc ++ s -> t
	c'.result = c.result
	}

pred show [s:Student,t:Tutor,c,c':Course]{
	inv[c] && addReg[s,t,c,c'] => inv[c']
}

run show for 1
//run show for 2
//run show for 3
//run show for 4
//run show for 5
//run show for 9
