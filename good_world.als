//module publication
sig Person {}
sig Book {author: Person}
sig Autobiography extends Book {}
fact {
	all disj b1,b2:Autobiography | b1.author!=b2.author
}
pred good_world(){
	all p: Person | some b: Book | b.author = p	
}
//run good_world for 1
//run good_world for 2
//run good_world for 3
//run good_world for 4
run good_world for 5
